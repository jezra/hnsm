// get an element by its id
function id(str) {
	return document.getElementById(str);
}

//print console output
function p(){
	console.log(arguments);
}

function round_to_decimal(number, accuracy) {
	return +(Math.round(number + ("e+"+accuracy))  + ("e-"+accuracy));
}
