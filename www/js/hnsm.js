/* global variables */
modem_data = [];
anytime_remaining = 0;
bonus_remaining = 0;
color = {normal: "green", alert: "orange", warning: "red"}
base_data_url= "http://api.statusmeter.hughesnet.com/clients/daily_usage";
first_data_process= true;

// get the modem data
function get_modem_data() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			process_modem_data( xhttp.responseText );
		}
	};
	xhttp.open("GET", "http://192.168.0.1/getdeviceinfo/info.bin", true);
	xhttp.send();
}

// get the usage data
function get_usage_data(days) {
	if(!days) {
		days = 30 ;
	}
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			process_usage_data( xhttp.responseText );
		}
	};
	//craft the URL
	url = base_data_url+"/"+modem_data['SAN']+"/"+days
	xhttp.open("GET", url, true);
	xhttp.send();
}

function process_modem_data(data) {
	lines = data.split("\n");
	for (var i = 0; i < lines.length; i++) {
		//get the line
		var line = lines[i];
		//does the line contain a "="?
		if (line.indexOf("=") != -1) {
			// split the line
			var key_val = line.split("=");
			modem_data[key_val[0]] = key_val[1];
		}
	}
	update_progress();
	//get new data in a minute
	setTimeout(get_modem_data, 60000);

	//is this the first time we are processing?
	if(first_data_process) {
		first_data_process= false;
		//kick off the fetching of usage data from the remote server
		get_usage_data();
	}
}

function process_usage_data(data) {
	data = JSON.parse(data);
	//parse the data to make the lines
	var upload_line = [];
	var download_line = [];
	var total_line = [];
	for(var i=0; i < data.length; i++) {
		node = data[i];
		date = node['STARTDATE'];
		upload_line.push([date, parseFloat(node['UPLOAD'])]);
		download_line.push([date,	parseFloat(node['DOWNLOAD'])]);
		total_line.push([date,	parseFloat(node['TOTAL'])]);

	}
	var plot = $.jqplot('plot', [total_line,download_line,upload_line], {
		title:'Data Usage',
		axes:{
			xaxis: {
				renderer: $.jqplot.DateAxisRenderer,
				tickOptions: {formatString:'%b %d'},
				tickInterval: '5 days'
			},
			yaxis: {
				min: 0,
				tickOptions: {formatString:'%i'},
			}
		},
		highlighter: {
			show: true,
			sizeAdjust: 7.5
		},
		seriesDefaults: {rendererOptions: {smooth: true} },
		series:[{lineWidth: 1, markerOptions: {size: 3, style: 'circle'}},
			{lineWidth: 1, markerOptions: {size: 3, style: 'circle'}},
			{lineWidth: 1, markerOptions: {size: 3, style:'circle'}}
		],
		grid: {
			drawGridLines: true,        // wether to draw lines across the grid or not.
			gridLineColor: '#cccccc',    // *Color of the grid lines.
			background: '#000',      // CSS color spec for background color of grid.
			borderColor: '#999999',     // CSS color spec for border around grid.
			borderWidth: 2.0,           // pixel width of border around grid.
			shadow: true,               // draw a shadow for grid.
			shadowAngle: 45,            // angle of the shadow.  Clockwise from x axis.
			shadowOffset: 1.5,          // offset from the line of the shadow.
			shadowWidth: 3,             // width of the stroke for the shadow.
			shadowDepth: 3,             // Number of strokes to make when drawing shadow.
																	// Each stroke offset by shadowOffset from the last.
			shadowAlpha: 0.07,           // Opacity of the shadow
			renderer: $.jqplot.CanvasGridRenderer,  // renderer to use to draw the grid.
			rendererOptions: {}         // options to pass to the renderer.  Note, the default
															// CanvasGridRenderer takes no additional options.
		},
	});
	//reprocess in a second
	setTimeout(get_usage_data, 1000*60*60);
}

function humanize_bytes( bytes ) {
	if(bytes > 1000) {
		bytes = round_to_decimal( (bytes/1000), 1);
		return bytes + " GB";
	} else {
		return bytes + " MB";
	}
}

function update_progress() {
	var remaining = modem_data['AnyTimeAllowanceRemaining'];
	//pass this data to the web wrapper
	pass_hover_string(remaining+" MB remaining");
	var total = modem_data['AnyTimePlanAllowance'];
	var anytime_percent = remaining * 100 / total;
	anytime_remaining = humanize_bytes(remaining);
	anytime_circle.anytime_remaining = anytime_remaining;
	anytime_circle.update(anytime_percent);
	//update the icon circle
	icon_circle.update(anytime_percent);

	//update the width of the data bar
	$("#bar_fill").width( 100-anytime_percent+"%" );
	if (anytime_percent < 15) {

	}

	if (anytime_percent < 5) {

	}

	var remaining = modem_data['BonusBytesAllowanceRemaining'];
	var total = modem_data['BonusBytesPlanAllowance'];
	var bonus_percent = remaining * 100 / total;
	bonus_remaining = humanize_bytes(remaining);
	bonus_circle.update(bonus_percent);

	if (bonus_percent < 15) {

	}
  	if (bonus_percent < 5) {

	}


	/* update the icon svg text */
	//get the 3rd svg
	var icon_svg = document.getElementsByTagName("svg")[2];
	//clone the node
	var icon_svg_clone = icon_svg.cloneNode(true);
	//create a tmp div
	var tmp = document.createElement("div");
	//append the icon to the tmp div
	tmp.appendChild(icon_svg_clone);
	//get the html in the tmp div
	var svg_string = tmp.innerHTML;
	pass_svg_string(svg_string);


	/* handle date stuff */
	var today = moment();
	id("date_today").innerHTML = today.format('MMMM D YYYY');
	var reset_date = moment(today).add( parseInt(modem_data['TimeLeftUntilRefill']) ,"m");
	id("date_reset").innerHTML = reset_date.format('MMMM D YYYY');

	/* determine when throttling will occur */
	//when was the previous reset?
	var previous_reset = moment(reset_date).add(-30,'d');
	//how many bytes have been used?
	var bytes_used = modem_data['AnyTimePlanAllowance'] - modem_data['AnyTimeAllowanceRemaining'];
	//how much time since last reset?
	var elapsed_minutes = today.diff(previous_reset,'minutes');
	//how much data is being used per minute?
	var bytes_per_minute = bytes_used/elapsed_minutes;
	//at this rate, how many minutes until bytes are exhausted?
	var projected_minutes_of_data_remaining = modem_data['AnyTimeAllowanceRemaining'] / bytes_per_minute;
	//what is the date of projected_minutes of data remaining ending?

	var projected_throttle = today.add(projected_minutes_of_data_remaining,'m');
	id("date_throttle").innerHTML = projected_throttle.format('MMMM D YYYY');

	var month_in_minutes = 43200;
	//what percentage of the month has passed?
	var percent = (elapsed_minutes * 100) / month_in_minutes;
	//update the bar_today width
	$("#bar_today").width( percent+"%");
}

function create_circles() {
	//create a test circle
	anytime_circle = Circles.create({
		id:           'anytime_circle',
		radius:       60,
		value:        0,
		maxValue:     100,
		width:        15,
		duration:     0,
		text:         function(){ return anytime_remaining;},
		colors:       ['#333', '#0d0'],
		styleWrapper: true,
		styleText:    false
	});
	bonus_circle = Circles.create({
		id:           'bonus_circle',
		radius:       60,
		value:        0,
		maxValue:     100,
		width:        15,
		duration:     0,
		text:         function(){ return bonus_remaining;},
		colors:       ['#333', '#0d0'],
		styleWrapper: true,
		styleText:    false
	});

	icon_circle = Circles.create({
		id:           'icon_circle',
		radius:       60,
		value:        0,
		maxValue:     100,
		width:        30,
		text:         '',
		colors:       ['#111', '#0d0'],
		wrpClass:     'icon_circle_wrapper',
		styleWrapper: true,
		duration:     0,
		styleText:    false
	});

}

function pass_svg_string(text) {
	pass_system_message("SVG_STRING", text);
}

function pass_hover_string(text) {
	pass_system_message("HOVER_TEXT", text);
}

function pass_system_message(type, text) {
	console.log(type+"::"+text);
}

window.addEventListener('load', function(){
	/* -- init -- */
	//create circles
	create_circles();
	//get data
	get_modem_data();
}, false);

